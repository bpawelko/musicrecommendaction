import pymysql
import os

from app.DATA_FILES import *
from app.DATA_FILES.SONGS import SongsDataFiles

#TODO zabezpieczenie żeby nie uzyc 2razy tylko przy instalacji
#TODO do poprawy sql, nie dodawac w petli!

def clean_reset_id():
    connection = pymysql.connect(host='localhost',
                                 user=os.getenv('DATABASE_USER'),
                                 password=os.getenv('DATABASE_USER_PASSWORD'),
                                 db=os.getenv('DATABASE_NAME'),
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            CLEANING_SQL = "DELETE FROM `song`"
            cursor.execute(CLEANING_SQL)
            connection.commit()

            RESET_ID_SQL = "ALTER TABLE `song` AUTO_INCREMENT = 1"
            cursor.execute(RESET_ID_SQL)
            connection.commit()
    except Exception as e:
        print(e)
        connection.rollback()
    finally:
        connection.close()


def insert_songs(songs_data_file: SongsDataFiles):
    connection = pymysql.connect(host='localhost',
                                 user=os.getenv('DATABASE_USER'),
                                 password=os.getenv('DATABASE_USER_PASSWORD'),
                                 db=os.getenv('DATABASE_NAME'),
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            INSERT_SQL = "INSERT INTO `song` (`filename`, `name`, `artist`, `genre`) VALUES (%s, %s, %s, %s)"
            for song_data_file in songs_data_file.playlist:
                cursor.execute(INSERT_SQL, (song_data_file.filename,
                                            song_data_file.name,
                                            song_data_file.artist,
                                            songs_data_file.genre_name)
                               )
            connection.commit()
            print(songs_data_file.genre_name + " INSERTING DONE")
    except Exception as e:
        print(e)
        connection.rollback()
    finally:
        connection.close()


clean_reset_id()
insert_songs(BLUES_SONGS)
insert_songs(COUNTRY_SONGS)
insert_songs(DISCO_SONGS)
insert_songs(HIPHOP_SONGS)
insert_songs(JAZZ_SONGS)
insert_songs(METAL_SONGS)
insert_songs(POP_SONGS)
insert_songs(REGGAE_SONGS)
insert_songs(ROCK_SONGS)
