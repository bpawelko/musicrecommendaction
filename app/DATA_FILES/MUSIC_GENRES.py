class MusicGenre:
    name: str
    genre_cover: str

    def __init__(self, name: str, genre_cover: str):
        self.name = name
        self.genre_cover = genre_cover


BLUES = MusicGenre(name="BLUES", genre_cover="static/pictures/blues.jpg")
COUNTRY = MusicGenre(name="COUNTRY", genre_cover="static/pictures/country.jpg")
DISCO = MusicGenre(name="DISCO", genre_cover="static/pictures/disco.jpg")
HIPHOP = MusicGenre(name="HIPHOP", genre_cover="static/pictures/hiphop.jpg")
JAZZ = MusicGenre(name="JAZZ", genre_cover="static/pictures/jazz.jpg")
METAL = MusicGenre(name="METAL", genre_cover="static/pictures/metal.jpg")
POP = MusicGenre(name="POP", genre_cover="static/pictures/pop.jpg")
REGGAE = MusicGenre(name="REGGAE", genre_cover="static/pictures/reggae.jpg")
ROCK = MusicGenre(name="ROCK", genre_cover="static/pictures/rock.jpg")

MUSIC_GENRES = [BLUES, COUNTRY, DISCO, HIPHOP, JAZZ, METAL, POP, REGGAE, ROCK]
