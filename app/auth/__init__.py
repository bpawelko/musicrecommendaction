from flask import Blueprint

blue_print = Blueprint('auth', __name__)

from app.auth import routes
