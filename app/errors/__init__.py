from flask import Blueprint

blue_print = Blueprint('errors', __name__)

from app.errors import handlers
