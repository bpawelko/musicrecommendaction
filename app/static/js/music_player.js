var audio;

//Hide pause button
$('#pause').hide();

//Initialize Audio and Previous User Rates
initAudio($('#playlist li:first-child'))
initRates()

function initRates() {
    $('#playlist .star-widget').each(function () {
        if ($(this).attr('rate') !== "None") {
            var radioButtonName = '#' + $(this).attr('song_id') + '-' + $(this).attr('rate')
            $(radioButtonName).prop({checked: true})
        }
    });
}

function initAudio(element) {
    var song = element.attr('song');
    var title = element.attr('title');
    var cover = element.attr('cover');
    var artist = element.attr('artist');
    audio = new Audio('static/music/' + song);

    if(!audio.currentTime) {
        $('#duration').html('0:00');
    }

    $('#audio-player .artist').text(artist);
    $('#audio-player .title').text(title);

    // Cover
    $('img.cover').attr('src', cover);

    $('#playlist li').removeClass('active');
    element.addClass('active');
}

//Play Button
$('#play').click(function() {
    audio.play();
    $('#play').hide();
    $('#pause').show();
    $('#duration').fadeIn(400);
    showDuration();
});

//Next Button
$('#next').click(function() {
    audio.pause();
    var next = $('#playlist li.active').nextAll(".li-song").first()
    if (next.length === 0) {
        next = $('#playlist li.active').prevAll(".li-song").last();
    }
    initAudio(next);
    audio.play();
    audio.volume = parseFloat($('#volume').val()/10);
    $('#play').hide();
    $('#pause').show();
    $('#duration').fadeIn(400);
    showDuration();
});

//Previous Button
$('#previous').click(function() {
    audio.pause();
    var previous = $('#playlist li.active').prevAll(".li-song").first();
    if (previous.length === 0) {
        previous = $('#playlist li.active').nextAll(".li-song").last();
    }
    initAudio(previous);
    audio.play();
    audio.volume = parseFloat($('#volume').val()/10);
    $('#play').hide();
    $('#pause').show();
    $('#duration').fadeIn(400);
    showDuration();
});

//Pause Button
$('#pause').click(function() {
    audio.pause();
    $('#pause').hide();
    $('#play').show();
});

//Stop Button
$('#stop').click(function() {
    audio.pause();
    audio.currentTime = 0;
    $('#pause').hide();
    $('#play').show();
    $('#duration').fadeOut(400);
});

//Time Duration
function showDuration() {
    $(audio).bind('timeupdate', function () {
        // Get seconds and minuts
        var sec = parseInt(audio.currentTime % 60);
        var min = parseInt((audio.currentTime/60) % 60)

        //To have 0.01 not 0.1
        if (sec < 10) {
            sec = '0' + sec;
        }

        $('#duration').html(min + '.' + sec);

        var value = 0;
        if (audio.currentTime > 0) {
            value = Math.floor((78/audio.duration)* audio.currentTime);
        }

        $('#progress').css('width', value+'%');

        if (audio.currentTime === audio.duration) {
                $('#play').show();
                $('#pause').hide();
                $('#progress').css('width', 0+'%');
        }
    });
}

//Volume
$('#volume').change(function() {
    audio.volume = parseFloat(this.value/10);
})

//Playlist on click
$('#playlist li').click(function () {
    audio.pause();
    initAudio($(this))
    audio.volume = parseFloat($('#volume').val()/10);
    audio.play()
    $('#play').hide();
    $('#pause').show();
    $('#duration').fadeIn(400);
    showDuration();
})

$('#playlist .star-widget input').click(function () {

    var song_id = $(this).parent().prevAll(".li-song").first().attr('song_id')
    $.ajax({
        data: {
            rate: this.value,
            song_id: song_id
        },
        type: 'POST',
        url: '/process'
    })
})

