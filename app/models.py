import jwt

from hashlib import md5

from app import db, login
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from time import time
from flask import current_app


class User(UserMixin, db.Model):
    id = db.Column(db.INTEGER, primary_key=True)
    username = db.Column(db.VARCHAR(64), index=True, unique=True)
    email = db.Column(db.VARCHAR(128), index=True, unique=True)
    password_hash = db.Column(db.VARCHAR(128))
    song_ratings = db.relationship('SongRating', backref='author', lazy='dynamic')
    about_me = db.Column(db.VARCHAR(128))
    last_seen = db.Column(db.DATETIME, default=datetime.utcnow())

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])['reset_password']
        except:
            return None
        return User.query.get(id)

    def __repr__(self):
        return f"<User {self.username} Email {self.email}>"


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class SongRating(db.Model):
    id = db.Column(db.INTEGER, primary_key=True)
    rate = db.Column(db.VARCHAR(128))
    timestamp = db.Column(db.DATETIME, index=True, default=datetime.utcnow())
    song_id = db.Column(db.INTEGER, db.ForeignKey('song.id'))
    user_id = db.Column(db.INTEGER, db.ForeignKey('user.id'))


class Song(db.Model):
    id = db.Column(db.INTEGER, primary_key=True)
    filename = db.Column(db.VARCHAR(128))
    name = db.Column(db.VARCHAR(256))
    artist = db.Column(db.VARCHAR(128))
    genre = db.Column(db.VARCHAR(128))


class Test(db.Model):
    id = db.Column(db.INTEGER, primary_key=True)
    name = db.Column(db.VARCHAR(128))
