from app.main import blue_print

from flask import render_template, flash, redirect, url_for, request, jsonify
from app import db
from app.main.forms import EditProfileForm
from app.models import User, Song, SongRating
from flask_login import current_user, login_required
from datetime import datetime
from app.DATA_FILES import *
from sqlalchemy import text


@blue_print.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@blue_print.route('/', methods=['GET', 'POST'])
@blue_print.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', title='Home')


@blue_print.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = [
        {'author': user, 'body': ''},
        {'author': user, 'body': ''}
    ]
    return render_template('user.html', user=user, posts=posts)


@blue_print.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('main.edit_profile'))

    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile', form=form)


@blue_print.route('/music_browser', methods=['GET'])
@login_required
def music_browser():
    return render_template('music_browser.html', title='Music', music_genres=MUSIC_GENRES)


@blue_print.route('/music_player', methods=['GET'])
@login_required
def music_player():
    songs = Song.query.filter_by(genre=ROCK.name).limit(20)
    all_rates = db.session.execute(
        text(
            "SELECT song.id AS _song_id, user_song_rating.song_id, user_song_rating.user_id, user_song_rating.rate FROM song "
            "LEFT JOIN (SELECT song_rating.song_id, song_rating.user_id, song_rating.rate FROM song_rating "
            "           WHERE song_rating.user_id = :user_id) user_song_rating "
            "ON song.id = user_song_rating.song_id "
            "WHERE song.genre = :genre "
            "LIMIT 20"),
        {"user_id": current_user.id, "genre": "ROCK"}).fetchall()

    return render_template('music_player.html', title='Music',
                           zipped_songs_rates=zip(songs, all_rates), cover=ROCK.genre_cover, user_id=current_user.id)


@blue_print.route('/process', methods=['POST'])
@login_required
def process():
    rate = request.form['rate']
    song_id = request.form['song_id']

    '#Checking if rate of this song for this user exists in database : yes/no : UPDATE/CREATE'
    song_rating = SongRating.query.filter_by(song_id=song_id, user_id=current_user.id).first()
    if song_rating:
        song_rating.rate = rate
        song_rating.timestamp = datetime.utcnow()
        db.session.commit()
    else:
        song_rating = SongRating(rate=rate, timestamp=datetime.utcnow(), song_id=song_id, user_id=current_user.id)
        db.session.add(song_rating)
        db.session.commit()
    return jsonify(status="OK")

